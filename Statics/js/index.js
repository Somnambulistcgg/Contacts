$(function(){
    $(".btn-sidenav").click(function () {
        if ($(this).find(".nav-bar").hasClass("nav-bar-animate")) {
            $(this).find(".nav-bar").removeClass("nav-bar-animate");
            $(".sidenav").removeClass("show-sidenav").addClass("hide-sidenav");
            $(".header-wrap .sidenav-mark").animate({
                opacity: 0,
            }, 500, function(){
                $(this).remove();
            });
            $('body').css({
                'overflow': 'auto'
            });
        } else {
            $(this).find(".nav-bar").addClass("nav-bar-animate");
            $(".sidenav").addClass("show-sidenav").removeClass("hide-sidenav");
            $(".header-wrap").append("<div class='sidenav-mark'></div>");
            $('body').css({
                'overflow': 'hidden'
            });
        }
    });

    $("#shutong_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#shutong").offset().top-60, }, 1000);
        return false;
    });

    $("#wudian_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#wudian").offset().top-60, }, 1000);
        return false;
    });

    $("#huacai_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#huacai").offset().top-55, }, 1000);
        return false;
    });

    $("#dilv_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#dilv").offset().top-55, }, 1000);
        return false;
    });

    $("#shengke_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#shengke").offset().top-55, }, 1000);
        return false;
    });

    $("#huangong_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#huangong").offset().top-55, }, 1000);
        return false;
    });

    $("#xinchuan_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#xinchuan").offset().top-55, }, 1000);
        return false;
    });

    $("#tiyuan_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#tiyuan").offset().top-60, }, 1000);
        return false;
    });

    $("#waiyuan_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#waiyuan").offset().top-60, }, 1000);
        return false;
    });

    $("#wenyuan_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#wenyuan").offset().top-55, }, 1000);
        return false;
    });

    $("#zhengzhi_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#zhengzhi").offset().top-55, }, 1000);
        return false;
    });

    $("#jiaoke_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#jiaoke").offset().top-55, }, 1000);
        return false;
    });

    $("#lishe_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#lishe").offset().top-55, }, 1000);
        return false;
    });

    $("#meiyuan_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#meiyuan").offset().top-55, }, 1000);
        return false;
    });

    $("#yinyue_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#yinyue").offset().top-55, }, 1000);
        return false;
    });

    $("#fayuan_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#fayuan").offset().top-55, }, 1000);
        return false;
    });

    $("#jingguan_a").click(function () {
        $(".btn-sidenav").click();
        $("html, body").animate({
            scrollTop: $("#jingguan").offset().top-55, }, 1000);
        return false;
    });
});